/*
 * Copyright (C) 2019 Sezer Gül (info@sezergul.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sezergul.security;

import com.sezergul.model.Account;
import com.sezergul.persistence.AccountDAO;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.Authorizer;
import io.dropwizard.auth.basic.BasicCredentials;
import java.util.Optional;

/**
 *
 * @author Sezer Gül (info@sezergul.com)
 */
public class AccountAuthenticator implements Authenticator<BasicCredentials, Account> {

    private final AccountDAO accountDao = new AccountDAO();

    @Override
    public Optional<Account> authenticate(BasicCredentials credentials) throws AuthenticationException {
        Account account = accountDao.getByEmail(credentials.getUsername());   
        if (account != null && credentials.getPassword().equals(account.getPassword())) {
            return Optional.of(account);
        }
        return Optional.empty();
    }    
    
}
