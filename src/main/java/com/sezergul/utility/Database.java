/*
 * Copyright (C) 2019 Sezer Gül (info@sezergul.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sezergul.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Sezer Gül (info@sezergul.com)
 */
public class Database {

    String user = "postgres";
    String password = "postgres";
    String url = "jdbc:postgresql://localhost:5432/sezergulapp" +
            "?autoReconnect=true&allowMultiQueries=true&useSSL=false";

    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException | SQLException e) {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");  
            LocalDateTime now = LocalDateTime.now(); 
            String returning = "WARN  [" + dtf.format(now) + "] com.sezergul.utility.Database: CANNOT CONNECT TO DATABASE!";                
            System.out.println(returning);
        }
        return connection;
    }
}
