/*
 * Copyright (C) 2019 Sezer Gül (info@sezergul.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sezergul;

import com.sezergul.security.AccountAuthenticator;
import com.sezergul.model.Account;
import com.sezergul.resources.AccountResource;
import com.sezergul.security.AccountAuthorizer;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

/**
 *
 * @author Sezer Gül (info@sezergul.com)
 */
public class SezerGulApplication extends Application<SezerGulConfiguration> {

    public static void main(final String[] args) throws Exception {
        new SezerGulApplication().run(args);
    }

    @Override
    public String getName() {
        return "SezerGul";
    }

    @Override
    public void initialize(final Bootstrap<SezerGulConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final SezerGulConfiguration configuration,
                    final Environment environment) {
        environment.jersey().register(AccountResource.class);
        environment.jersey().register(new AuthDynamicFeature(
            new BasicCredentialAuthFilter.Builder<Account>()
                .setAuthenticator(new AccountAuthenticator())
                .setAuthorizer(new AccountAuthorizer())
                .setRealm("SUPER SECRET STUFF")
                .buildAuthFilter()));
        environment.jersey().register(RolesAllowedDynamicFeature.class);
        environment.jersey().register(new AuthValueFactoryProvider.Binder<>(Account.class));
    }

}
