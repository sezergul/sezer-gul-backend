/*
 * Copyright (C) 2019 Sezer Gül (info@sezergul.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sezergul.resources;

import com.sezergul.model.Account;
import com.sezergul.service.AccountService;
import io.dropwizard.auth.Auth;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Sezer Gül (info@sezergul.com)
 */
@Singleton
@Path("/account")
public class AccountResource {
    
    public DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");  
    public LocalDateTime now = LocalDateTime.now(); 
    private AccountService accountService;

    @Inject
    public AccountResource() {
        this.accountService = new AccountService();   
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public Collection<Account> getAll(@Auth Account account) {
        System.out.println("RSRC  [" + dtf.format(now) + "] com.sezergul.resource.AccountResource: @GET/account.");
        return accountService.getAll();
    }
    
    @GET  
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"0"})
    @Path("/{id}")
    public Account get(@Auth Account account, @PathParam("id") int id){ 
        System.out.println("RSRC  [" + dtf.format(now) + "] com.sezergul.resource.AccountResource: @GET/account/" + id + ".");
        return accountService.get(id);
    }
    
    @GET  
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    @Path("/me")
    public Account getMe(@Auth Account account){
        System.out.println("RSRC  [" + dtf.format(now) + "] com.sezergul.resource.AccountResource: @GET/account/me.");
        return accountService.get(account.getId());
    }
}
