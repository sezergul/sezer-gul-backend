/*
 * Copyright (C) 2019 Sezer Gül (info@sezergul.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sezergul.persistence;

import com.sezergul.model.Account;
import com.sezergul.utility.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;

/**
 *
 * @author Sezer Gül (info@sezergul.com)
 */
@Singleton
public class AccountDAO {
    
    public DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");  
    public LocalDateTime now = LocalDateTime.now(); 
    public Database database;
    public PreparedStatement query;
    public List<Account> accounts;
    volatile static AccountDAO accountDao;

    public AccountDAO() {
        this.database = new Database();
    }
    
    public List<Account> getAll(){
        Connection connection = database.getConnection();
        accounts = new ArrayList<>();
        ResultSet resultSet = null;
        try {
            query = connection.prepareStatement(
                    "SELECT acc_id, acc_firstname, acc_lastname, acc_gender, acc_birthdate, acc_email, acc_password, acc_role, acc_deactivated " +
                    "FROM account;");
            resultSet = query.executeQuery();
        } catch (SQLException e) {    
            String returning = "ERROR [" + dtf.format(now) + "] com.sezergul.persistence.AccountDAO: Error starting getAll() function.";
            System.out.println(returning);
        } try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    accounts.add(new Account(
                            resultSet.getInt("acc_id"), 
                            resultSet.getString("acc_firstname"), 
                            resultSet.getString("acc_lastname"), 
                            resultSet.getInt("acc_gender"),
                            resultSet.getString("acc_birthdate"), 
                            resultSet.getString("acc_email"),
                            resultSet.getString("acc_password"),
                            resultSet.getInt("acc_role"),
                            resultSet.getInt("acc_deactivated")));
                }
            }
        } catch (SQLException e) {
            String returning = "ERROR [" + dtf.format(now) + "] com.sezergul.persistence.AccountDAO: Error executing getAll() function.";
            System.out.println(returning);
        } finally {
            try {
                query.close();
                connection.close();
            } catch (SQLException e) {
                String returning = "ERROR [" + dtf.format(now) + "] com.sezergul.persistence.AccountDAO: Error closing getAll() function.";
                System.out.println(returning);
            }
        }
        return accounts;
    }
    
    public Account get(int id){
        Account account = null;
        for(Account temp:getAll()){
            if(temp.getId() == id){
                account = temp;
            }
        }
        return account;
    }
    
     public Account getByEmail(String email) {
        Connection connection = database.getConnection();
        ResultSet resultSet = null;
        Account account = new Account();
        try {
            query = connection.prepareStatement(
                    "SELECT acc_id, acc_firstname, acc_lastname, acc_gender, acc_birthdate, acc_email, acc_password, acc_role, acc_deactivated " +
                    "FROM account WHERE acc_email = ?;");
            query.setString(1, email);
            resultSet = query.executeQuery();
        } catch(SQLException e) {
            String returning = "ERROR [" + dtf.format(now) + "] com.sezergul.persistence.AccountDAO: Error starting getByEmail() function.";
            System.out.println(returning);
        } try {
            if (resultSet != null) {
                while (resultSet.next()){
                    account.setId(resultSet.getInt("acc_id"));
                    account.setFirstname(resultSet.getString("acc_firstname"));
                    account.setLastname(resultSet.getString("acc_lastname"));
                    account.setGender(resultSet.getInt("acc_gender"));
                    account.setBirthdate(resultSet.getString("acc_birthdate"));
                    account.setEmail(resultSet.getString("acc_email"));
                    account.setPassword(resultSet.getString("acc_password"));
                    account.setRole(resultSet.getInt("acc_role"));
                    account.setDeactivated(resultSet.getInt("acc_deactivated"));
                }
            }
        } catch(SQLException e) {
            String returning = "ERROR [" + dtf.format(now) + "] com.sezergul.persistence.AccountDAO: Error executing getByEmail() function.";
            System.out.println(returning);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                String returning = "ERROR [" + dtf.format(now) + "] com.sezergul.persistence.AccountDAO: Error closing getByEmail() function.";
                System.out.println(returning);
            }
        }
        return account;
    }
    
    public static AccountDAO getInstance() throws SQLException {
        if (accountDao == null) {
            synchronized (AccountDAO.class) {
                if (accountDao == null) {
                    accountDao = new AccountDAO();
                }
            }
        }
        return accountDao;
    }
}
