/*
 * Copyright (C) 2019 Sezer Gül (info@sezergul.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sezergul.service;

import com.sezergul.model.Account;
import com.sezergul.persistence.AccountDAO;
import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.NotFoundException;

/**
 *
 * @author Sezer Gül (info@sezergul.com)
 */
@Singleton
public class AccountService {
    
    private AccountDAO accountDao;

    @Inject
    public AccountService() {
        this.accountDao = new AccountDAO();
    }

    public Collection<Account> getAll() {
        return accountDao.getAll();
    }
    
    public Account get(int id) {
        return accountDao.get(id);
    }
}
